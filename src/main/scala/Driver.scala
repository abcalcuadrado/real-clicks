import java.awt.{Robot, Toolkit}
import java.awt.datatransfer.StringSelection
import java.awt.event.{InputEvent, KeyEvent}
import java.util.concurrent.{ThreadLocalRandom, TimeUnit}

import scala.concurrent.ExecutionContext.Implicits.global
import akka.actor.Actor
import akka.actor.Actor.Receive
import DriverProtocol._

import scala.concurrent.duration.Duration
import scala.util.Random

/**
  * Created by vbot on 2/1/17.
  */

object DriverProtocol {

  case class TypeURL()

  case class SecondClickForTypeUrl()

  case class Scroll(pixels: Int)

  case class ReturnScroll(pixels: Int)

  case class ClickOnNewIdentityMenu()

  case class ClickOnNewIdentity()

  case class ClickOnAdd()

  case class Init()

  case class ResetTor()

}

object Driver {
  val URLS = Seq(
    "https://www.youtube.com/watch?v=Rl8YL1AwkJc",
    "https://www.youtube.com/watch?v=VODT_ASlCOw",
    "https://www.youtube.com/watch?v=BlyYOsyNXcg",
    "https://www.youtube.com/watch?v=hISy6oxJClE"
    //    "http://bigdatashow.weebly.com/big-data-aws/partitioning-data-para-analizar-big-data-con-athena",
    //    "http://bigdatashow.weebly.com/big-data-aws/creando-tablas-en-amazon-athena-para-analisis-de-big-data",
    //    "http://bigdatashow.weebly.com/big-data-aws/analisis-de-big-data-en-amazon-athena-y-s3"
  )
}

class Driver extends Actor {
  val bot = new Robot()

  private def clickOn(x: Int, y: Int): Unit = {
    bot.mouseMove(x, y)
    bot.mousePress(InputEvent.BUTTON1_MASK)
    bot.mouseRelease(InputEvent.BUTTON1_MASK)
  }

  override def receive: Receive = {

    case ResetTor() =>
      clickOn(931, 428)
      clickOn(931, 428)
      bot.keyPress(KeyEvent.VK_CONTROL)
      bot.keyPress(KeyEvent.VK_C)
      bot.keyRelease(KeyEvent.VK_C)
      bot.keyRelease(KeyEvent.VK_CONTROL)
      bot.keyRelease(KeyEvent.VK_UP)
      bot.keyPress(KeyEvent.VK_ENTER)
      bot.keyRelease(KeyEvent.VK_ENTER)
//      context.system.scheduler.scheduleOnce(Duration.create(5, TimeUnit.SECONDS), self, TypeURL())
    case TypeURL() =>
      clickOn(306, 70)
      context.system.scheduler.scheduleOnce(Duration.create(50, TimeUnit.MILLISECONDS), self, SecondClickForTypeUrl())

    case SecondClickForTypeUrl() =>
      clickOn(306, 70)
      val url = Random.shuffle(Driver.URLS.toList).head
      val stringSelection2 = new StringSelection(url)
      val clipboard2 = Toolkit.getDefaultToolkit.getSystemClipboard
      clipboard2.setContents(stringSelection2, null)
      bot.keyPress(KeyEvent.VK_CONTROL)
      bot.keyPress(KeyEvent.VK_V)
      bot.keyRelease(KeyEvent.VK_V)
      bot.keyRelease(KeyEvent.VK_CONTROL)
      bot.keyPress(KeyEvent.VK_ENTER)
      bot.keyRelease(KeyEvent.VK_ENTER)
      context.system.scheduler.scheduleOnce(Duration.create(ThreadLocalRandom.current().nextInt(40, 60), TimeUnit.SECONDS), self, Scroll(
        ThreadLocalRandom.current().nextInt(0, 500)
      ))



    case Scroll(pixels) =>
      bot.mouseMove(520, 282)
      bot.setAutoDelay(100)
      bot.mouseWheel(pixels)
      context.system.scheduler.scheduleOnce(Duration.create(ThreadLocalRandom.current().nextInt(2, 10), TimeUnit.SECONDS), self, ReturnScroll(pixels))

    case ReturnScroll(pixels) =>
      bot.setAutoDelay(100)
      bot.mouseWheel(-pixels)
      val decision = ThreadLocalRandom.current().nextBoolean()
      println(decision)
      //if(decision) {
      println("ClickOnNewIdentityMenu")
      context.system.scheduler.scheduleOnce(Duration.create(ThreadLocalRandom.current().nextInt(2, 10), TimeUnit.SECONDS), self, ClickOnNewIdentityMenu())
    //      } else {
    //        println("ClickOnAdd")
    //        context.system.scheduler.scheduleOnce(Duration.create(30, TimeUnit.SECONDS), self, ClickOnAdd())
    //      }

    //    case ClickOnAdd()
    //    =>
    //      clickOn(631, 516)
    //      context.system.scheduler.scheduleOnce(Duration.create(30, TimeUnit.SECONDS), self, ClickOnNewIdentityMenu())

    case ClickOnNewIdentityMenu()
    =>
      clickOn(82, 75)
      context.system.scheduler.scheduleOnce(Duration.create(500, TimeUnit.MILLISECONDS), self, ClickOnNewIdentity())

    case ClickOnNewIdentity()
    =>
      clickOn(82, 101)
      context.system.scheduler.scheduleOnce(Duration.create(8, TimeUnit.SECONDS), self, TypeURL())
  }
}
