
import java.util.concurrent.TimeUnit

import DriverProtocol.{ResetTor, TypeURL}
import akka.actor.{Actor, ActorSystem, Props}

import scala.concurrent.ExecutionContext.Implicits.global
import scala.concurrent.duration.Duration
/**
  * Created by vbot on 1/30/17.
  */
object Main extends App {


  val system = ActorSystem()
  val actor = system.actorOf(Props[Driver])
  actor ! TypeURL()
  system.scheduler.schedule(Duration.create(15, TimeUnit.MINUTES), Duration.create(15, TimeUnit.MINUTES), actor, ResetTor())

}

//
//object DriverProtocol {
//
//  case class ClickOnLinuxMenu()
//
//  case class ClickOnDebianMenu()
//
//  case class ClickOnApplicationsMenu()
//
//  case class ClickOnShellsMenu()
//
//  case class ClickOnBash()
//
//  case class ClickOnBashFileMenu1()
//
//  case class ClickOnBashNewTab1()
//
//  case class ClickOnBashFileMenu2()
//
//  case class ClickOnBashNewTab2()
//
//  case class ClickOnBashTab1Header()
//
//  case class ClickOnBashTab1Focus()
//
//  case class LaunchTor1()
//
//  case class ClickOnBashTab2Header()
//
//  case class ClickOnBashTab2Focus()
//
//  case class LaunchTor2()
//
//  case class ClickOnBashTab3Header()
//
//  case class ClickOnBashTab3Focus()
//
//  case class LaunchTor3()
//
//
//}
//
//
//class Driver extends Actor {
//  val bot = new Robot()
//
//  private def clickOn(x: Int, y: Int): Unit = {
//    bot.mouseMove(x, y)
//    bot.mousePress(InputEvent.BUTTON1_MASK)
//    bot.mouseRelease(InputEvent.BUTTON1_MASK)
//  }
//
//
//  private def launchTorOnShell(): Unit = {
////    val cdTor = "cd tor-browser_en-US/Browser/"
//    bot.keyPress(KeyEvent.VK_C)
//    bot.keyPress(KeyEvent.VK_C)
//    bot.keyPress(KeyEvent.VK_D)
//    bot.keyRelease(KeyEvent.VK_V)
//    bot.keyRelease(KeyEvent.VK_CONTROL)
//    bot.keyPress(KeyEvent.VK_ENTER)
//    bot.keyRelease(KeyEvent.VK_ENTER)
////    val lauchTor = "./start-tor-browser"
////    val stringSelection2 = new StringSelection(lauchTor)
////    val clipboard2 = Toolkit.getDefaultToolkit.getSystemClipboard
////    clipboard2.setContents(stringSelection2, null)
////    bot.keyPress(KeyEvent.VK_CONTROL)
////    bot.keyPress(KeyEvent.VK_V)
////    bot.keyRelease(KeyEvent.VK_V)
////    bot.keyRelease(KeyEvent.VK_CONTROL)
////    bot.keyPress(KeyEvent.VK_ENTER)
////    bot.keyRelease(KeyEvent.VK_ENTER)
//
//  }
//
//  private def schedule(time: Long, message: Any): Unit = {
//    context.system.scheduler.scheduleOnce(Duration.create(time, TimeUnit.MILLISECONDS), self, message)
//  }
//
//
//  override def receive: Receive = {
//    case ClickOnLinuxMenu() =>
//      clickOn(21, 627)
//      schedule(2000, ClickOnDebianMenu())
//
//    case ClickOnDebianMenu() =>
//      clickOn(57, 416)
//      schedule(2000, ClickOnApplicationsMenu())
//
//    case ClickOnApplicationsMenu() =>
//      clickOn(238, 416)
//      schedule(2000, ClickOnShellsMenu())
//
//    case ClickOnShellsMenu() =>
//      clickOn(375, 503)
//      schedule(2000, ClickOnBash())
//
//    case ClickOnBash() =>
//      clickOn(572, 506)
//      schedule(2000, ClickOnBashFileMenu1())
//
//    case ClickOnBashFileMenu1() =>
//      clickOn(382, 236)
//      schedule(2000, ClickOnBashNewTab1())
//
//    case ClickOnBashNewTab1() =>
//      clickOn(426, 280)
//      schedule(2000, ClickOnBashFileMenu2())
//
//    case ClickOnBashFileMenu2() =>
//      clickOn(382, 201)
//      schedule(2000, ClickOnBashNewTab2())
//
//    case ClickOnBashNewTab2() =>
//      clickOn(424, 239)
//      schedule(2000, LaunchTor1())
//    case LaunchTor1() =>
//      clickOn(419, 228)
//      launchTorOnShell()
//
//
//  }
//}
